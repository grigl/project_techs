var Fancybox = function() {
    $('.fancybox').fancybox({
        padding: 40,
        minHeight: 'auto',
        tpl : {
            closeBtn : '<div class="modal-close"></div>'
        },
        helpers: {
            overlay : {
                opacity: 0.5, 
                locked: false
            }
        }
    });
}

$(function(){
    Fancybox();
});